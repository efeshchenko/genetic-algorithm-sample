'''
Sample genetic algorithm
'''
import sys, time, string, random, logging, argparse
from random import randint

LOGGER = logging.getLogger(' ')
GENES = string.printable
GOAL = "Hello world! Genetic Algorithm Python Vesrion"
MUTATE_TURNS = 5

def timer(func):
    "simple timer decorator"
    def timed(*args, **kwargs):
        "closure"
        start = time.time()
        result = func( *args, **kwargs)
        end = time.time()
        LOGGER.info("Time:   {0}".format(end-start))
        return result
    return timed

class GeneticCode:
    "Class for single genetic code"
    def __init__(self, dna="", goal=GOAL):
        if not dna:
            code = [random.choice(GENES)]*len(GOAL)
            self.dna = "".join(code)
        else:
            self.dna = dna
        self.goal = goal

    def fitness(self):
        """
        calculates how close is dna to GOAL <<< this called "fitness function"
        diff = 0 if conditions are satisfied
        """
        diff = 0
        for index, gene in enumerate(self.dna):
            if gene != self.goal[index]:
                diff -= 1
        return diff

    def mutate(self):
        """
        mutate dna
        """
        _dna = self.dna
        cnt = 0
        while cnt < MUTATE_TURNS:
            index = randint(0, len(_dna)-1)
            if _dna[index] != self.goal[index]:
                _dna = _dna[:index] + random.choice(GENES) + _dna[index+1:]
            cnt += 1
        self.dna = _dna

    def replicate(self, another_dna):
        """
        breed 2 dna sequences
        cut one, cut two and mix it together
        return offspring dna string
        """
        part = randint(0, len(self.dna)-1)
        return "".join(self.dna[0:part] + another_dna.dna[part:])

class GenePool():
    "Class for dealing with banch of GeneticCodes"
    def __init__(self, pool_size, goal=GOAL ):
        self.pool_size = pool_size
        self.pool = [GeneticCode(goal=goal)]*self.pool_size
        self.goal = goal

    def get_random(self):
        "Get random element from pool"
        return self.pool[randint(0, len(self.pool)-1)]

    def darvin(self, winners=0.1):
        """
        choose only good dna sequences
        winners - part of population to breed
        """
        all_fitness = [[item.fitness(), item] for item in self.pool]
        sorted_fitness = sorted(all_fitness, key=lambda x: x[0], reverse=True)
        new_pool = [item[1] for item in sorted_fitness]
        self.pool = new_pool[:int(round(self.pool_size * winners))]

        new_pool = []
        while len(new_pool) < self.pool_size:
            replicated = self.get_random().replicate(self.get_random())
            new_code = GeneticCode(dna=replicated, goal=self.goal)
            new_pool.append(new_code)
        self.pool = new_pool

    def evolution(self, turns=1000):
        "Evalute pool"
        iterations = 0
        while (iterations < turns) and (self.pool[0].dna != self.goal):
            for index in xrange(len(self.pool)-1):
                self.pool[index].mutate()
            self.darvin()
            LOGGER.info(self.pool[0].dna)
            iterations += 1

        return iterations

def env_init():
    "Init environment"
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    parser = argparse.ArgumentParser(description='Simple genetic algorithm')
    parser.add_argument("-s", help="Size of a population", type=int)
    args = parser.parse_args()
    return args

@timer
def main():
    "Main part"
    args = env_init()
    size = args.s if args.s else 1000
    population = GenePool(goal=GOAL, pool_size = size)
    steps = population.evolution()
    LOGGER.info("Steps:  {0}".format(steps))
    return steps

main()
